
module Resolution(sat, tau, valid, V, F(..), Statement, L(..), C, CSet, Clash) where

type V = String
data F = Atom V
       | Neg  F
       | Conj F F                  
       | Disy F F     
       | Imp  F F
       deriving(Eq)     

type Statement = ([F],F)

data L = LP V | LN V
         deriving (Eq)
type C    = [L]
type CSet = [C]
type Clash = (C,L,C,L)

-----------------------------------------
-- Variables auxiliares
-----------------------------------------
p = LN "p"
q = LN "q"
r = LN "r"
s = LN "s"
t = LN "t"
np = LP "p"
nq = LP "q"
nr = LP "r"
ns = LP "s"
nt = LP "t"

type V = String
data F = Atom V
       | Neg  F
       | Conj F F                  
       | Disy F F     
       | Imp  F F
       deriving(Eq)     

type Statement = ([F],F)

data L = LP V | LN V
         deriving (Eq)
type C    = [L]
type CSet = [C]
type Clash = (C,L,C,L)

-----------------------------------------
-- Variables auxiliares
-----------------------------------------
p = LN "p"
q = LN "q"
r = LN "r"
s = LN "s"
t = LN "t"
np = LP "p"
nq = LP "q"
nr = LP "r"
ns = LP "s"
nt = LP "t"

premisas = [(Neg (Atom "p" `Conj` Atom "q"))]
conclusion = Neg (Atom "p") `Conj` Neg(Atom "q")
stat = (premisas, conclusion)
form = mergeForm premisas `Conj` Neg(conclusion)
form2 = Neg(mergeForm premisas `Conj` conclusion)
c = f2CSet form
clashes = findClashes c

test :: CSet -> IO() 
test a = print a

g1 = c
g2 = resolveClashes (findClashes g1)
g3 = mergeSet g1 g2
g4 = resolveClashes (findClashes g3)
g5 = mergeSet g3 g4
g6 = resolveClashes (findClashes g5)
g7= mergeSet g5 g6


-----------------------------------------
-- Funciones principales
-----------------------------------------
-- Pos: retorna True si la formula es SAT, o False si es UNSAT
sat :: F -> Bool
sat a = undefined

-- Pos: retorna True si la formula es Tautología, o False en caso contrario
tau :: F -> Bool
tau = undefined

-- Pos: retorna True si el razonamiento es válido, o False en caso contrario
valid :: Statement -> Bool
valid = undefined

-----------------------------------------
-- Formulas y Clausulas
-----------------------------------------
-- Pos: convierte una formula a un conjunto de clausulas
f2CSet :: F -> CSet
f2CSet f = cnf2CSet (f2cnf f)

-- Pre: recibe una formula en FNC
-- Pos: convierte la FNC a un conjunto de clausulas
cnf2CSet :: F -> CSet
cnf2CSet (Atom v) = [[LN v]]
cnf2CSet (Neg (Atom v)) = [[LP v]]
cnf2CSet (a `Disy` b) = [merge (head(cnf2CSet a)) (head(cnf2CSet b))]
cnf2CSet (a `Conj` b) = (cnf2CSet a) `mergeSet` (cnf2CSet b)

-- Pos: convierte una formula a FNC
f2cnf :: F -> F
f2cnf = distr . pushNeg . sustImp

sustImp :: F -> F
sustImp (Atom v)     = (Atom v)
sustImp (Neg a)      = Neg (sustImp a)
sustImp (a `Conj` b) = (sustImp a) `Conj` (sustImp b)
sustImp (a `Disy` b) = (sustImp a) `Disy` (sustImp b)
sustImp (a `Imp`  b) = (Neg (sustImp a)) `Disy` (sustImp b) 

pushNeg :: F -> F
pushNeg (Atom v)           = (Atom v)
pushNeg (Neg (a `Disy` b)) = (pushNeg (Neg a)) `Conj` (pushNeg (Neg b))
pushNeg (Neg (a `Conj` b)) = (pushNeg (Neg a)) `Disy` (pushNeg (Neg b))
pushNeg (Neg (Neg a))      = pushNeg a
pushNeg (Neg a)            = Neg (pushNeg a)
pushNeg (a `Conj` b)       = (pushNeg a) `Conj` (pushNeg b)
pushNeg (a `Disy` b)       = (pushNeg a) `Disy` (pushNeg b)

distr :: F -> F
distr (a `Disy` b) = distr' ((distr a) `Disy` (distr b))
  where 
  distr' (a `Disy` (b `Conj` c)) = (distr' (a `Disy` b)) `Conj` (distr' (a `Disy` c))
  distr' ((a `Conj` b) `Disy` c) = (distr' (a `Disy` c)) `Conj` (distr' (b `Disy` c))
  distr' a                       = a
distr (a `Conj` b) = (distr a) `Conj` (distr b)
distr a            = a  


-----------------------------------------
-- Procedimiento de Resolución
-----------------------------------------
-- Pre: recibe un conjunto de clausulas
-- Pos: si es SAT,   retorna el conjunto de clausulas saturado
--      si es UNSAT, retorna un conjunto de clausulas incluyendo la clausula vacía
resolveCSet :: CSet -> CSet
resolveCSet [] = []
resolveCSet a
  | length (mergeSet a (resolveClashes (findClashes a))) > length a
    = resolveCSet (mergeSet a (resolveClashes (findClashes a))) 
  | otherwise = a

resolveClashes :: [Clash] -> CSet
resolveClashes [] = []
resolveClashes ((a,b,c,d):xs) =  mergeSet [(merge (filter (/=b) a) (filter (/=d) c))] (resolveClashes xs)

-- Esta función retorna todos los conflictos existentes en un conjunto de cláusulas
findClashes :: CSet -> [Clash]
findClashes [] = []
findClashes [x] = []
findClashes (x:xs) = (findClash x xs) ++ (findClashes xs)

findClash :: C -> CSet -> [Clash]
findClash a [x] = case (length (clash a x)) > 0 of {
  True -> [(a, (head (clash a x)), x, compl (head (clash a x)))];
  False -> []
}
findClash a (x:xs) = (findClash a [x])++(findClash a xs)

-----------------------------------------
-- Funciones auxiliares
-----------------------------------------

compl :: L -> L
compl (LP x) = LN x
compl (LN x) = LP x

-- Devuelve la lista de literales pertenecientes al clash
-- lista vacia: no hay conflicto
-- lista un elemento: un conflicto
-- lista mas de un elemento remover conflicto por ser trivial
clash :: C -> C -> [L]
clash _ [] = []
clash (x:xs) (y:ys) = (filter (==(compl y)) (x:xs))++(clash (x:xs) (ys))

merge :: C -> C -> C
merge [] a = a
merge [x] a = x : (filter (/=x) a)
merge (x:xs) a = merge xs (merge [x] a)

mergeSet :: CSet -> CSet -> CSet
mergeSet [] a = a
mergeSet (x:xs) a
  | any (eqC x) a = mergeSet xs a
  | otherwise = x:(mergeSet xs a)
  
mergeForm :: [F] -> F
mergeForm [x] = x
mergeForm (x:xs) = x `Conj` (mergeForm xs)

incC :: C -> C -> Bool
incC [] a = True
incC (x:xs) a = (elem x a) && (incC xs a)

eqC :: C -> C -> Bool
eqC a b = (incC a b) && (incC b a)

----------------------------------------------------------------------------------
-- Pretty Printing
instance Show F where
  show (Atom v)       = v
  show (Neg (Neg a))  = "~" ++ show (Neg a)
  show (Neg (Atom v)) = "~ " ++ show (Atom v)
  show (Neg a)        = "~ (" ++ show a ++ ")"
  show (a `Conj` b)   = "(" ++ show a ++ ") /\\ (" ++ show b ++ ")"
  show (a `Disy` b)   = "(" ++ show a ++ ") \\/ (" ++ show b ++ ")"
  show (a `Imp` b)    = "(" ++ show a ++ ") --> (" ++ show b ++ ")"

instance Show L where  
  show (LP v)  = "~" ++ v
  show (LN v)  = v 